package desktop.controller;

import desktop.exception.CouldNotInitializeException;
import javafx.scene.layout.GridPane;

/**
 * Created with IntelliJ IDEA.
 * User: Lorand
 * Date: 5/31/14
 * Time: 7:07 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SecureEyeController {
    void start(OnPINCompletedCallback onPINCompletedCallback1) throws CouldNotInitializeException;

    GridPane getView(int width, int height);
}
