package desktop.controller;

import desktop.exception.CouldNotInitializeException;
import desktop.MainClass;
import desktop.view.NumpadView;
import com.theeyetribe.client.GazeManager;
import com.theeyetribe.client.IGazeListener;
import com.theeyetribe.client.data.GazeData;
import javafx.scene.layout.GridPane;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Lorand
 * Date: 5/31/14
 * Time: 6:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class SecureEyeControllerImpl implements SecureEyeController {

    private static final double MAX_LEFT_RIGHT_DIFF = 220;
    public static final double MAX_THRESHOLD = 30;
    private static final int POINT_BUFFER_SIZE = 40;
    private static final int MIN_CONSECUTIVE_POINTS = 35;
    private static final long MIN_TIME_ON_BUTTON = 400;
    private static final long MIN_TIME_ON_SAME_BUTTON = 900;
    private static final double X_RESOLUTION = 1366;
    private static final double Y_RESOLUTION = 768;
    public static final int BTN_CANCEL = -1;
    public static final int BTN_CLEAR = -2;
    public static final int BTN_ERROR = -3;
    public static final int ACCEPTED_STATE = 7;
    private static final int MAX_CONS_ELIM = 5;
    public static final String BEEP_MP3 = "resources/beep-06.wav";
    private OnPINCompletedCallback onPINCompletedCallback;
    private final List<ViewPoint> currentViewPoints;
    private int consecutiveEliminated = 0;
    private int prevButton = -100;
    private NumpadView numpadView;
    private final GazeManager gm;
    private IGazeListener listener;
    private String results = "";

    public SecureEyeControllerImpl() {
        this.currentViewPoints = new ArrayList<ViewPoint>();
        gm = GazeManager.getInstance();
    }

    @Override
    public GridPane getView(int width, int height) {
        this.numpadView = new NumpadView(width, height);
        return numpadView;
    }

    @Override
    public void start(OnPINCompletedCallback onPINCompletedCallback) throws CouldNotInitializeException {
        this.onPINCompletedCallback = onPINCompletedCallback;
        clearTriesState();
        initEyeTracker();
    }

    private void clearTriesState() {
        numpadView.setCompletedCharacterCount(0);
        results = "";
    }

    public void initEyeTracker() throws CouldNotInitializeException {
        deactivateIfRequired();
        clearState();
        boolean success = gm.activate(GazeManager.ApiVersion.VERSION_1_0, GazeManager.ClientMode.PUSH);
        if (success) {
            listener = ensureGazeListener();
            gm.addGazeListener(listener);

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    deactivateIfRequired();
                }
            });
        } else {
            throw new CouldNotInitializeException("Failed to connect to eye tracker");
        }
    }

    private void deactivateIfRequired() {
        if (listener != null) {
            gm.removeGazeListener(listener);
            gm.deactivate();
        }
    }

    private IGazeListener ensureGazeListener() {
        if (listener == null) {
            listener = new IGazeListener() {
                @Override
                public void onGazeUpdate(GazeData gazeData) {
                    onFrameReceived(gazeData);
                }
            };
        }
        return listener;
    }

    private void onFrameReceived(GazeData gazeData) {
        synchronized (currentViewPoints) {
            if (gazeData.state == ACCEPTED_STATE) {
                double lx = gazeData.leftEye.smoothedCoordinates.x;
                double ly = gazeData.leftEye.smoothedCoordinates.y;
                double rx = gazeData.rightEye.smoothedCoordinates.x;
                double ry = gazeData.rightEye.smoothedCoordinates.y;

                if (isDifferenceOk(lx, rx) && isDifferenceOk(ly, ry)) {
                    double avgX = (lx + rx) / 2d;
                    double avgY = (ly + ry) / 2d;
                    ViewPoint e = new ViewPoint(avgX, avgY, gazeData.timeStamp);
                    currentViewPoints.add(e);

                    if (currentViewPoints.size() >= POINT_BUFFER_SIZE) {
                        if (eliminateErrors(currentViewPoints)) {
                            consecutiveEliminated++;
                        }

                        if (enoughInfoReceived(currentViewPoints)) {
                            ViewPoint avg = getAvg(currentViewPoints);
                            int button = getButton(avg);
                            boolean canProceed = true;
                            if (button == prevButton) {
                                canProceed = enoughInfoReceivedForTheSameButton(currentViewPoints);
                            }
                            if (canProceed) {
                                prevButton = button;
                                doOnButtonPressed(button);
                                clearState();
                            }
                        }
                        if (consecutiveEliminated > MAX_CONS_ELIM) {
                            clearState();
                        }
                    }

                } else {
                    consecutiveEliminated = 0;
                }

            } else {
                currentViewPoints.clear();
            }
        }
    }

    private void clearState() {
        currentViewPoints.clear();
        consecutiveEliminated = 0;
    }

    private boolean enoughInfoReceivedForTheSameButton(List<ViewPoint> currentViewPoints) {
        return (currentViewPoints.get(currentViewPoints.size() - 1).getTimeStamp() - currentViewPoints.get(0).getTimeStamp()) >= MIN_TIME_ON_SAME_BUTTON;
    }

    private void doOnButtonPressed(int button) {
        System.out.print("############### Hurray!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  ");
        switch (button) {
            case BTN_ERROR: {
                System.out.println("UNKNOWN BUTTON");
                break;
            }
            case BTN_CLEAR: {
                playSound(BEEP_MP3);
                System.out.println("Clear Pressed");
                clearResults();
                break;
            }
            case BTN_CANCEL: {
                playSound(BEEP_MP3);
                System.out.println("CANCEL BUTTON pressed");
                deactivateIfRequired();
                onPINCompletedCallback.onCancel();
                break;
            }
            default: {
                playSound(BEEP_MP3);
                System.out.println("Button pressed:" + button);
                if (getResultsCount() < 3) {
                    addResult(button);
                } else {
                    addResult(button);
                    deactivateIfRequired();
                    onPINCompletedCallback.didGetPIN(results);
                }
                break;
            }
        }
    }

    private void addResult(int button) {
        results += String.valueOf(button);
        numpadView.setCompletedCharacterCount(getResultsCount());
    }

    private int getResultsCount() {
        return results.length();
    }

    private void clearResults() {
        results = "";
        numpadView.setCompletedCharacterCount(0);
    }

    private int getButton(ViewPoint avg) {
        double x = avg.getX();
        double y = avg.getY();

        double xButtonLength = X_RESOLUTION / 3;
        double yOffset = Y_RESOLUTION * 4 / 18;
        double yButtonLength = Y_RESOLUTION * 14 / 18 / 4;
        if (y < yOffset){
            return BTN_ERROR;
        }
        if (x <= xButtonLength) {
            if (y <= yOffset + yButtonLength) {
                return 1;
            } else if (y <= yOffset + yButtonLength * 2) {
                return 4;
            } else if (y <= yOffset + yButtonLength * 3) {
                return 7;
            } else if (y <= yOffset + yButtonLength * 4) {
                return BTN_CLEAR;
            }
        } else if (x <= xButtonLength * 2) {
            if (y <= yOffset + yButtonLength) {
                return 2;
            } else if (y <= yOffset + (yButtonLength * 2)) {
                return 5;
            } else if (y <= yOffset + (yButtonLength * 3)) {
                return 8;
            } else if (y <= yOffset + (yButtonLength * 4)) {
                return 0;
            }
        } else if (x <= xButtonLength * 3) {
            if (y <= yOffset + yButtonLength) {
                return 3;
            } else if (y <= yOffset + (yButtonLength * 2)) {
                return 6;
            } else if (y <= yOffset + (yButtonLength * 3)) {
                return 9;
            } else if (y <= yOffset + (yButtonLength * 4)) {
                return BTN_CANCEL;
            }
        }
        return BTN_ERROR;
    }

    private boolean enoughInfoReceived(List<ViewPoint> currentViewPoints) {
        int size = currentViewPoints.size();
        if (size >= MIN_CONSECUTIVE_POINTS) {
            return (currentViewPoints.get(size - 1).getTimeStamp() - currentViewPoints.get(0).getTimeStamp()) >= MIN_TIME_ON_BUTTON;
        }
        return false;
    }

    private class ViewPoint {
        private final double x;
        private final double y;
        private final long timeStamp;

        private ViewPoint(double x, double y) {
            this.x = x;
            this.y = y;
            this.timeStamp = 0;
        }

        public ViewPoint(double x, double y, long timeStamp) {
            this.x = x;
            this.y = y;
            this.timeStamp = timeStamp;
        }

        private double getX() {
            return x;
        }

        private double getY() {
            return y;
        }

        public long getTimeStamp() {
            return timeStamp;
        }
    }

    private ViewPoint getAvg(List<ViewPoint> points) {
        double avgX = 0d;
        double avgY = 0d;

        for (ViewPoint point : points) {
            avgX += point.getX();
            avgY += point.getY();
        }
        int size = points.size();
        avgX /= size;
        avgY /= size;
        return new ViewPoint(avgX, avgY);
    }

    private boolean eliminateErrors(List<ViewPoint> points) {
        int originalSize = points.size();
        double currentThreshold = MAX_THRESHOLD + 1;
        while (currentThreshold > MAX_THRESHOLD && points.size() > 2) {
            ViewPoint avg = getAvg(points);
            ViewPoint worstPoint = null;
            double maxDiff = 0L;
            double currentDiff = 0;
            for (ViewPoint currentPoint : points) {
                currentDiff = Math.abs(currentPoint.getX() - avg.getX());
                if (currentDiff > maxDiff) {
                    maxDiff = currentDiff;
                    worstPoint = currentPoint;
                }
                currentDiff = Math.abs(currentPoint.getY() - avg.getY());
                if (currentDiff > maxDiff) {
                    maxDiff = currentDiff;
                    worstPoint = currentPoint;
                }
            }
            if (maxDiff >= MAX_THRESHOLD && worstPoint != null) {
                points.remove(worstPoint);
            }
            currentThreshold = maxDiff;
        }
        return points.size() != originalSize;
    }

    private boolean isDifferenceOk(double lx, double rx) {
        return Math.abs(lx - rx) < MAX_LEFT_RIGHT_DIFF;
    }

    private void playSound(final String url) {
        new Thread(new Runnable() {
            // The wrapper thread is unnecessary, unless it blocks on the
            // Clip finishing; see comments.
            public void run() {
                try {
                    Clip clip = AudioSystem.getClip();
                    AudioInputStream inputStream = AudioSystem.getAudioInputStream(
                            MainClass.class.getResourceAsStream(url));
                    clip.open(inputStream);
                    clip.start();
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }
        }).start();
    }
}
