package desktop.controller;

import desktop.exception.CouldNotInitializeException;
import desktop.util.socket.ConnectionSocketUtil;
import desktop.view.InfoView;
import desktop.view.StartingFrame;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * User: DAFny
 * Date: 6/1/14
 * Time: 2:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class LoginController extends Application {
    private static final String PREDEFINED_PIN = "1563";
    private static final int MAX_NR_OF_TRIES = 3;
    private static final long RESTART_AUTH_TIMEOUT = 5000;
    private static final long SWITCH_VIEW_DELAY = 5000;
    private Stage stage;
    private Scene authScene;

    private Scene infoScene;

    private int nrOfTries;
    private InfoView infoView;
    private SecureEyeController controller;
    private OnPINCompletedCallback completedCallback;
    private StartingFrame m_startingFrame;

    public void initializare() {
        launch();
    }

    @Override
    public void start(final Stage stage) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        m_startingFrame = new StartingFrame();
        m_startingFrame.setVisible(true);
        GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().setFullScreenWindow(m_startingFrame);

        this.stage = stage;
        infoView = new InfoView();
        this.infoScene = new Scene(infoView, 1366, 768);
        controller = new SecureEyeControllerImpl();
        authScene = new Scene(controller.getView(1366, 768));
        completedCallback = buildResultCallback();
        ConnectionSocketUtil connectionSocketUtil = new ConnectionSocketUtil(4567);
        String userData = connectionSocketUtil.start();
        TransactionContext.INSTANCE.setUsername(userData.substring(0, userData.indexOf(':')));

        m_startingFrame.setVisible(false);
        showScene(authScene);
        controller.start(completedCallback);
    }

    private OnPINCompletedCallback buildResultCallback() {
        return new OnPINCompletedCallback() {

            @Override
            public void didGetPIN(String pin) {
                if (isPinOk(pin)) {
                    onPinOk();
                } else {
                    doOnBadPin();
                }
            }

            @Override
            public void onCancel() {
                showInfoView("You have canceled the transaction!",70);
                restart();
            }

            @Override
            public void onGetButton(double v, double v2) {
            }
        };
    }

    private boolean isPinOk(String pin) {
        return PREDEFINED_PIN.equals(pin);
    }


    private void onPinOk() {
        showInfoView("Welcome " + TransactionContext.INSTANCE.getUsername() + ". \n\rYou have been logged in successfully.",70);
        restart();
    }

    private void showScene(final Scene scene) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                stage.setScene(scene);
                stage.setFullScreen(true);
                stage.show();
            }
        });
    }


    private void showInfoView(final String s) {
        System.out.println(s);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                infoView.setText(s);
                showScene(infoScene);
            }
        });
    }

    private void showInfoView(final String s, final int size) {
        System.out.println(s);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                infoView.setText(s);
                infoView.setTextSize(size);
                showScene(infoScene);
            }
        });
    }

    private void doOnBadPin() {
        nrOfTries++;
        if (nrOfTries < MAX_NR_OF_TRIES) {
            retryAuth();
        } else {
            showCardBlockedView();
        }
    }

    private void showCardBlockedView() {
        showInfoView("You entered a wrong pin number " + MAX_NR_OF_TRIES + " times. Your card is now blocked");
        restart();
    }

    private void restart() {
        clearState();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            start(new Stage());
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                });
            }
        }, RESTART_AUTH_TIMEOUT);
    }

    private void clearState() {
        nrOfTries = 0;
    }

    private void retryAuth() {
        showInfoView("You entered a wrong pin. You have " + (MAX_NR_OF_TRIES - nrOfTries) + " try(s) left. " +
                "\n\r (You will be redirected to the authentication page in  " + RESTART_AUTH_TIMEOUT / 1000 + " seconds)",45);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                startAuth();
            }
        }, RESTART_AUTH_TIMEOUT);
    }

    private void startAuth() {
        try {
            showScene(authScene);
            controller.start(completedCallback);
        } catch (CouldNotInitializeException e) {
            e.printStackTrace();
        }
    }


}
