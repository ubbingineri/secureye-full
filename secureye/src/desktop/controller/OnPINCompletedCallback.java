package desktop.controller;

/**
 * Created with IntelliJ IDEA.
 * User: Lorand
 * Date: 5/31/14
 * Time: 7:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface OnPINCompletedCallback {
    public void didGetPIN(String pin);
    public void onCancel();

    void onGetButton(double x, double y);
}
