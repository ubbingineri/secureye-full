package desktop.controller;

import desktop.view.StartingFrame;

/**
 * Created with IntelliJ IDEA.
 * User: Lorand
 * Date: 6/1/14
 * Time: 7:29 AM
 * To change this template use File | Settings | File Templates.
 */
public enum TransactionContext {
    INSTANCE;
    private String username;
    private StartingFrame startView;

    public void setUsername(String username){

        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void startView(StartingFrame m_startingFrame) {
        this.startView = m_startingFrame;
    }

    public StartingFrame getStartView() {
        return startView;
    }
}
