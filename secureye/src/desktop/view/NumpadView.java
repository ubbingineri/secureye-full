package desktop.view;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Lorand
 * Date: 6/1/14
 * Time: 1:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class NumpadView extends GridPane {
    private static String[] NUMPAD = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "Clear", "0", "Cancel"};
    private ArrayList<Label> labelList = new ArrayList<Label>();
    private int numpadHeight;
    private int numpadWidth;
    private int numbersEntered = 0;

    public NumpadView(int width, int height) {
        setStyle("-fx-background-color:#DBDBDB;");
        setPrefSize(width, height);
        setMaxSize(width, height);
        numpadHeight = height - (height / 6) - (height / 18);
        numpadWidth = width;

        addRow(0, getPassPane(width, height / 6));
        addRow(1, getNumPane(numpadWidth, numpadHeight));
    }

    public void setCompletedCharacterCount(final int count) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 4; i++) {
                    if (count > i) {
                        labelList.get(i).setText("*");
                    } else {
                        labelList.get(i).setText("_");
                    }
                }
            }
        });
    }

    private Pane getPassPane(int width, int height) {
        GridPane pane = new GridPane();
        pane.setVgap(2);
        for (int i = 1; i <= 4; i++) {
            Label label;
            label = new Label("_");
            labelList.add(label);
            label.setFont(new Font("MS Sans Serif", height * 0.4));
            label.setMinHeight(height);
            label.setMinWidth(width / 4);
            label.setMaxHeight(height);
            label.setMaxWidth(width / 4);
            label.setStyle("-fx-background-color:#FFFFFF;");
            label.setAlignment(Pos.CENTER);
            pane.add(label, i, 0);

            // separator
            label = new Label();
            label.setMinHeight(height / 3);
            label.setMinWidth(width / 4);
            label.setMaxHeight(height / 3);
            label.setMaxWidth(width / 4);
            label.setStyle("-fx-background-color:#88C387;");
            pane.add(label, i, 1);
        }
        return pane;
    }

    private Pane getNumPane(int width, int height) {
        GridPane gridpane = new GridPane();
        gridpane.setHgap(3);
        gridpane.setVgap(3);
        gridpane.setPrefSize(width, height);
        gridpane.setMaxSize(width, height);

        int count = 0;
        for (int i = 1; i <= 4; i++) {
            for (int j = 1; j <= 3; j++) {
                Button btn = new Button(NUMPAD[count++]);
                double labelHeight = (double) height / 4;
                btn.setFont(new Font("MS Sans Serif", labelHeight * 0.30));
                btn.setMinHeight(labelHeight - 5);
                btn.setMinWidth(width / 3 - 4);
                btn.setMaxHeight(labelHeight - 5);
                btn.setMaxWidth(width / 3 - 4);
                btn.setStyle("-fx-background-color:#FFFFFF;");
                btn.setAlignment(Pos.CENTER);
                btn.setFocusTraversable(false);
                gridpane.add(btn, j, i);
            }
        }
        return gridpane;
    }

}
