package desktop.view;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;

/**
* Created with IntelliJ IDEA.
* User: Lorand
* Date: 6/1/14
* Time: 7:37 AM
* To change this template use File | Settings | File Templates.
*/
public class InfoView extends BorderPane {

    private final Label label;

    public InfoView() {
        setStyle("-fx-background-color: #88C387;");
        label = new Label();
        label.setStyle("-fx-text-fill: #ffffff;");

        label.setFont(new Font(50));
        label.setAlignment(Pos.CENTER);

        setCenter(label);
    }

    public void setText(String text) {
        label.setText(text);
    }

    public void setTextSize(int textSize) {
        label.setFont(new Font(textSize));
    }
}
