package desktop.exception;

/**
 * Created with IntelliJ IDEA.
 * User: Lorand
 * Date: 5/31/14
 * Time: 7:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class CouldNotInitializeException extends Exception{
    public CouldNotInitializeException(String message) {
        super(message);
    }
}
