package desktop.repository.impl;

import desktop.model.UserStatistics;
import desktop.repository.IUserStatisticsRepository;
import desktop.util.persistence.IDatabaseConnection;
import desktop.util.persistence.impl.DatabaseConnectionImpl;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserStatisticsRepositoryImpl implements IUserStatisticsRepository {
    private IDatabaseConnection databaseConnection;

    public UserStatisticsRepositoryImpl() {
        databaseConnection = new DatabaseConnectionImpl();
    }

    public UserStatisticsRepositoryImpl(IDatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public List<UserStatistics> get() {
        List<UserStatistics> userStatisticsList = new ArrayList<UserStatistics>();

        databaseConnection.open();
        Connection connection = null;

        try {
            connection = databaseConnection.getConnection();

            String select = "SELECT * FROM UserStatistics";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(select);

            while (resultSet.next()) {
                userStatisticsList.add(new UserStatistics(resultSet.getInt("userId"), resultSet.getTimestamp("timestamp"),
                        resultSet.getString("inputDuration"), resultSet.getString("inputPin"), resultSet.getBoolean("successfulAuthentication")));
            }

            return userStatisticsList;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return userStatisticsList;
    }

    @Override
    public void save(UserStatistics userStatistics) {

        databaseConnection.open();
        Connection connection = null;

        try {
            connection = databaseConnection.getConnection();

            String insert = "INSERT INTO UserStatistics(timestamp, inputTimestamp, inputPin, successfulAuthentication) VALUES(?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(insert);
            preparedStatement.setTimestamp(1, userStatistics.getTimestamp());
            preparedStatement.setString(2, userStatistics.getInputDuration());
            preparedStatement.setString(3, userStatistics.getInputPin());
            preparedStatement.setBoolean(4, userStatistics.getSuccessfulAuthentication());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(UserStatistics userStatistics) {
        databaseConnection.open();

        Connection connection = null;

        try {
            connection = databaseConnection.getConnection();

            String update = "UPDATE UserStatistics SET timestamp=?, inputTimestamp=?, inputPin=?, " +
                    "memoryFreeSpace=? WHERE userId = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(update);
            preparedStatement.setTimestamp(1, userStatistics.getTimestamp());
            preparedStatement.setString(2, userStatistics.getInputDuration());
            preparedStatement.setString(3, userStatistics.getInputPin());
            preparedStatement.setBoolean(4, userStatistics.getSuccessfulAuthentication());
            preparedStatement.setInt(5, userStatistics.getUserId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void delete(UserStatistics userStatistics) {
        databaseConnection.open();
        Connection connection = null;

        try {
            connection = databaseConnection.getConnection();

            String delete = "DELETE UserStatistics WHERE userId = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(delete);
            preparedStatement.setInt(1, userStatistics.getUserId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
