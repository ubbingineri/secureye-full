package desktop.util.socket;

import java.io.*;
import java.net.Socket;

public class CreditCardHandler implements Runnable {
    private Socket socket;

    public CreditCardHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println("Connection to server established !");

        System.out.println("Client: " + socket.getInetAddress().getHostAddress() + ", address " + socket.getRemoteSocketAddress().toString() + " successfully connected!");

        InputStream in = null;
        try {
            in = socket.getInputStream();
            OutputStream out = socket.getOutputStream();

            ObjectInput objectInput = new ObjectInputStream(in);
            ObjectOutput objectOutput = new ObjectOutputStream(out);

            String username = (String) objectInput.readObject();
            String creditCard = (String) objectInput.readObject();

            System.out.println("Username data: " + username);
            System.out.println("Credit card data: " + creditCard);
            String result = "SUCCESS";

            objectOutput.writeObject(result);
            objectOutput.flush();
        }catch (IOException e){
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
