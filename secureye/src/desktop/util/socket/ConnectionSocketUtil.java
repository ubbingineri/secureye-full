package desktop.util.socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionSocketUtil {
    private ServerSocket serverSocket;
    private int port;

    public ConnectionSocketUtil(int port) {
        this.port = port;
    }

    public String start() {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Could not listen to port: " + e);
        }

        System.out.println("Server successfully started.");
        String username = "";
        String creditCard = "";
        while (true) {
            try {
                Socket socket = serverSocket.accept();

                System.out.println("Connection to server established !");

                System.out.println("Client: " + socket.getInetAddress().getHostAddress() + ", address " + socket.getRemoteSocketAddress().toString() + " successfully connected!");

                InputStream in = null;
                try {
                    in = socket.getInputStream();
                    OutputStream out = socket.getOutputStream();

                    ObjectInput objectInput = new ObjectInputStream(in);
                    ObjectOutput objectOutput = new ObjectOutputStream(out);

                    username = (String) objectInput.readObject();
                    creditCard = (String) objectInput.readObject();

                    System.out.println("Username data: " + username);
                    System.out.println("Credit card data: " + creditCard);
                    String result = "SUCCESS";

                    objectOutput.writeObject(result);
                    objectOutput.flush();
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            } catch (IOException e) {
                System.out.println("Error accepting socket: " + e);
            }
        }
        String response = username + ":" + creditCard;
        return response;
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
