package desktop.util.persistence;

import java.sql.Connection;

public interface IDatabaseConnection {

    void open();

    void close();

    Connection getConnection();
}
