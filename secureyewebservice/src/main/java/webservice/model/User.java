package webservice.model;

import java.io.Serializable;

public class User implements Serializable {
    private Integer id;

    private String username;

    private String pin;

    private String creditCard;

    public User(String username, String pin, String creditCard) {
        this.username = username;
        this.pin = pin;
        this.creditCard = creditCard;
    }

    public User(Integer id, String username, String pin, String creditCard) {
        this.id = id;
        this.username = username;
        this.pin = pin;
        this.creditCard = creditCard;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }
}
