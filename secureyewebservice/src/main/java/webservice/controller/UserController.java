package webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import webservice.service.IUserService;
import webservice.service.impl.UserServiceImpl;

@Controller
public class UserController {
    private IUserService userService;

    @RequestMapping("/userCreditCard")
    public
    @ResponseBody
    String userCreditCard(@RequestParam(value = "username", required = true) String username,
                          @RequestParam(value = "creditCard", required = true) String creditCard) {
        userService = new UserServiceImpl();

        System.out.println("Credit Card received data: " + creditCard);

        if (userService.processCreditCard(username, creditCard).equals("SUCCESS")) {
            return "{\"result\" : \"SUCCESS\"}";
        } else {
            return "{\"result\" : \"FAILURE\"}";
        }
    }
}