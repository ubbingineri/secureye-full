package com.example.secureyeandroid;

import com.example.secureyeandroid.service.CreditCardService;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity {
	private ImageButton imageButton;
	private CreditCardService creditCardService;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		addListenerOnButton();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void addListenerOnButton() {
		 
		imageButton = (ImageButton) findViewById(R.id.imageButton1);
 
		imageButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				creditCardService = new CreditCardService();
				
				String username = "John Smith";
				String creditCard = "1234567";
				
				creditCardService.processCreditCard(username, creditCard);
 
			   Toast.makeText(MainActivity.this,"Credit Card data sent!", Toast.LENGTH_SHORT).show();
			}
		});
	}

}
