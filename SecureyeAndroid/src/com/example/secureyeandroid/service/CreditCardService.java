package com.example.secureyeandroid.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class CreditCardService {
	private String url = "http://10.155.163.106:8080/userCreditCard?";
	private URI uri;

	public void processCreditCard(String username, String creditCard) {
		try {
			this.url += "username=" + URLEncoder.encode(username, "UTF-8") + "&";
			this.url += "creditCard=" + URLEncoder.encode(creditCard, "UTF-8");

			uri = new URI(url);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		Log.i("URL: ", uri.toASCIIString());
		Thread serviceThread = new Thread(new Runnable() {
			@Override
			public void run() {
				InputStream inputStream = retrieveStream(uri.toString());
			}
		});
		serviceThread.start();
	}

	private InputStream retrieveStream(String url) {
		DefaultHttpClient client = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet(url);

		try {
			HttpResponse getResponse = client.execute(getRequest);
			final int statusCode = getResponse.getStatusLine().getStatusCode();

			if (statusCode != HttpStatus.SC_OK) {
				Log.w(getClass().getSimpleName(), "Error " + statusCode
						+ " for URL " + url);
				return null;
			}

			HttpEntity getResponseEntity = getResponse.getEntity();
			return getResponseEntity.getContent();
		} catch (IOException e) {
			getRequest.abort();
			Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
		}

		return null;
	}
}
